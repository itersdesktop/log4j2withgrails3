package log4j2withgrails3

import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.core.LoggerContext

class BootStrap {

    def init = { servletContext ->
        LoggerContext context = (LoggerContext) LogManager.getContext(false)

        String userHome =  System.getProperty("user.home")
        String pathname = ""
        if (grails.util.Environment.isDevelopmentMode()) {
            pathname = userHome + "/.myConfigurations/log4j2-dev.xml"
        } else {
            pathname = userHome + "/.myConfigurations/log4j2-prod.xml"
        }
        File file = new File(pathname)
        // this will force a reconfiguration
        context.setConfigLocation(file.toURI())
    }

    def destroy = {

    }

    static String getUserAppDirectory() {
        String osName = System.getProperty("os.name");
        System.out.println("os = " + osName);
        if (osName.contains("Mac")) {
            return  "target/.JCal/logs/JCal-log.log";
        }
        else {
            return  "target/JCal/logs/JCal-log.log";
        }
    }
}
