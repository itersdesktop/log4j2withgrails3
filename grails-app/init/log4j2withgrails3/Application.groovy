package log4j2withgrails3

import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

class Application extends GrailsAutoConfiguration {
    private static final Logger logger = LogManager.getLogger(Application.class.getName())
    static void main(String[] args) {
        logger.info("The application is starting...")
        GrailsApp.run(Application, args)
        logger.info("The application has been started!")
    }
}